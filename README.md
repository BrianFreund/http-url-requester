# README #

Copy the code from HTTP URL REQUESTER and paste it into a new SmartThings App created on graph.api.smartthings.com using the "From Code" choice.

### What is this repository for? ###

A SmartThings app that can be used to send url requests to any IP on your network and allows switches to trigger the sending of the URLs whether turned on or off.
Version 1.0

### Who do I talk to? ###

Brian Freund (contact on the SmartThings forum (https://community.smartthings.com/t/local-wifi-integration-of-milight-limitlessled-easyblub-using-spare-android-phone/31819) or through here)